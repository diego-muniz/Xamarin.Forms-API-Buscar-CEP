﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using Newtonsoft.Json;

namespace ConsumirApiXAMARIN
{
  public partial class MainPage : ContentPage
  {
    public MainPage()
    {
      InitializeComponent();
    }

    async void BtnBuscar_Clicked(object sender, EventArgs e)
    {
      try
      {
        string cep = txtCEP.Text;
        await buscarCep(cep);
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }

    private async Task buscarCep(string cep)
    {
      try
      {
        BuscaCep buscaCep = new BuscaCep();
        Endereco endereco = await buscaCep.buscarCep(cep);
        lblLogradouro.Text = string.Format($"Logradouro: {endereco.lougradouro}");
        lblComplemento.Text = string.Format($"Complemento: {endereco.complemento}");
        lblBairro.Text = string.Format($"Bairro: {endereco.bairro}");
        lblLocalidade.Text = string.Format($"Localidade: {endereco.localidade}");
        lblUF.Text = string.Format($"UF: {endereco.uf}");
        lblUnidade.Text = string.Format($"Unidade: {endereco.unidade}");
        lblIBGE.Text = string.Format($"IBGE: {endereco.ibge}");
        lblGIA.Text = string.Format($"GIA: {endereco.gia}");
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }
  }
}
