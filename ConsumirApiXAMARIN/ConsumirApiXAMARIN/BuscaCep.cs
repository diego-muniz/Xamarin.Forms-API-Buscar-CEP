﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

namespace ConsumirApiXAMARIN
{
  class BuscaCep : IBuscaCep
  {
    public async Task<Endereco> buscarCep(string Cep)
    {
      try
      {
        var client = new HttpClient();
        string cep = Cep;
        var json = await client.GetStringAsync($"https://viacep.com.br/ws/{cep}/json/");
        var dados = JsonConvert.DeserializeObject<Endereco>(json);
        return dados;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }
  }
}
