﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumirApiXAMARIN
{
  interface IBuscaCep
  {
      Task<Endereco> buscarCep(string cep);
  }
}
