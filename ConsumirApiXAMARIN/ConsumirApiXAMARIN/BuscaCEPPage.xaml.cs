﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ConsumirApiXAMARIN
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class BuscaCEPPage : ContentPage
  {
    public BuscaCEPPage()
    {
      InitializeComponent();
    }

    async void BtnBuscar_Clicked(object sender, EventArgs e)
    {
      var client = new HttpClient();
      string cep = txtCEP.Text;
      var json = await client.GetStringAsync($"https://viacep.com.br/ws/{cep}/json/");
      var dados = JsonConvert.DeserializeObject<Endereco>(json);

      lblLogradouro.Text = dados.lougradouro;
      lblComplemento.Text = dados.complemento;
      lblBairro.Text = dados.bairro;
      lblLocalidade.Text = dados.localidade;
      lblUF.Text = dados.uf;
      lblUnidade.Text = dados.unidade;
      lblIBGE.Text = dados.ibge;
      lblGIA.Text = dados.gia;

    }
  }
}